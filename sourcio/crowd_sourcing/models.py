from django.db import models

#############
# COLLECTIF #
#############
class Collectif(models.Model):
    nom = models.CharField(max_length = 30)
    url = models.URLField(null = True, blank = True)
    date_creation = models.TimeField(null = True, blank = True)

    def __repr__(self):
        return "{nomClasse}({id!s}) - {nom}".format(nomClasse = __class__.__name__, id = self.id, nom = self.nom)
    def __str__(self):
        return self.nom

##########
# CHAINE #
##########
class ChaineSexe(models.Model):
    valeur = models.IntegerField(null = True, blank = True)
    description = models.CharField(max_length = 50, null = True, blank = True)

class ChaineDomaine(models.Model):
    valeur = models.IntegerField(null = True, blank = True)
    description = models.CharField(max_length = 50, null = True, blank = True)

class Chaine(models.Model):
    nom = models.CharField(max_length = 64)
    url = models.CharField(max_length = 64, default = "", blank = True)
    date_creation = models.DateField(null = True, blank = True)
    collectif = models.ForeignKey(Collectif, on_delete = models.SET_NULL, null = True, blank = True)
    sexe_presentateur = models.ForeignKey(ChaineSexe, on_delete = models.SET_NULL, null = True, blank = True)
    domaine = models.ForeignKey(ChaineDomaine, on_delete = models.SET_NULL, null = True, blank = True)
    observation = models.TextField(default = "", blank = True)
    mise_a_jour = models.DateField(auto_now = True, null = True, blank = True)

    #abonnements = models.IntegerField(null = True, blank = True)

    def __repr__(self):
        return "{nomClasse}({id!s}) - {nom}".format(nomClasse = __class__.__name__, id = self.id, nom = self.nom)

    def __str__(self):
        return self.nom

class ChaineAbonnement(models.Model):
    chaine = models.ForeignKey(Chaine, on_delete = models.CASCADE)
    date = models.DateField(null = True, blank = True)
    nombre = models.IntegerField(null = True, blank = True)

#########
# VIDEO #
#########
class Video(models.Model):
    yt_id = models.CharField(max_length = 11) # Also URL
    date_publication = models.DateField(null = True, blank = True)
    titre = models.CharField(max_length = 90, default = "", blank = True)
    chaine = models.ForeignKey(Chaine, on_delete = models.PROTECT, null = True, blank = True)
    vulgarisation = models.BooleanField(default = True)
    sourcee = models.BooleanField(default = False)
    vues = models.BigIntegerField(null = True, blank = True)
    duree = models.TimeField(null = True, blank = True)
    description = models.TextField(default = "", blank = True)

    mise_a_jour = models.DateField(auto_now = True, null = True, blank = True)
    # Commentaires ?

    def __repr__(self):
        return "{nomClasse}({id!s}) - {nom}".format(nomClasse = __class__.__name__, id = self.id, nom = self.titre)

    def __str__(self):
        return "{chaine}: {titre}".format(chaine = self.chaine, titre = self.titre)

##########
# SOURCE #
##########
class Source(models.Model):
    nom = models.CharField(max_length = 100)
    timestamp_creation = models.DateTimeField(auto_now_add = True)
    video = models.ForeignKey(Video, on_delete = models.PROTECT, null = True, blank = True)
    seulement_citee = models.BooleanField(null = True, blank = True)
    source_personnelle = models.BooleanField(null = True, blank = True)
    time_code = models.TimeField(null = True, blank = True)
    description = models.TextField(default = "", blank = True)

    type_source = models.CharField(max_length = 50, default = "", blank = True)
    auteur_source = models.CharField(max_length = 50, default = "", blank = True)
    data_publication = models.DateField(null = True, blank = True)
    lieu = models.CharField(max_length = 50, default = "", blank = True)
    espace_inscription_disciplinaire = models.CharField(max_length = 50, default = "", blank = True)
    lieu_acces_publication = models.CharField(max_length = 50, default = "", blank = True)
    lieu_implantation_publication = models.CharField(max_length = 50, default = "", blank = True)

    mise_a_jour = models.DateField(auto_now = True, null = True, blank = True)

    def __repr__(self):
        return "{nomClasse}({id!s}) - {nom}".format(nomClasse = __class__.__name__, id = self.id, nom = self.nom)

    def __str__(self):
        return "{video} - [{nom}]".format(video = self.video, nom = self.nom)

class SourceURL(models.Model):
    source = models.ForeignKey(Source, on_delete = models.PROTECT)
    url = models.URLField(null = True, blank = True)
    info = models.TextField(null = True, blank = True)

    def __repr__(self):
        return "{nomClasse}({id!s}) - {nom}".format(nomClasse = __class__.__name__, id = self.id, nom = self.url)

    def __str__(self):
        return "{source} ({url})".format(source = self.source, url = self.url)
