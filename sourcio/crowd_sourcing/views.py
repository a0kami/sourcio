from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.db.models import Sum

from .models import Chaine, ChaineAbonnement, Collectif, Source, SourceURL, Video

# Create your views here.
def index(request):
    video_list = Video.objects.filter(vulgarisation = True).order_by('-date_publication')[:6]
    source_list = Source.objects.order_by('-timestamp_creation')[:6]

    actuel = Video.objects.filter(vulgarisation = True).filter(sourcee = True).count()
    total_vulgarisation = Video.objects.filter(vulgarisation = True).count()
    total = Video.objects.count()
    progression = {
        'actuel': actuel,
        'total_vulgarisation': total_vulgarisation,
        'total': total,
        'pourcentage' : actuel / total_vulgarisation * 100,
        }
    context = {'video_list': video_list, 'source_list': source_list, 'progression': progression}

    return render(request, 'crowd_sourcing/index.html', context)

# Lister les chaines ou voir les détails d'une
def chaine(request, chaine_id = None):
    if chaine_id == None:
        return render(request, 'crowd_sourcing/chaines.html', {'chaine': Chaine.objects.all().order_by('nom')})

    chaine = get_object_or_404(Chaine, pk=chaine_id)
    stats = {
        "vues": Video.objects.filter(chaine=chaine_id).filter(vulgarisation = True).aggregate(Sum('vues')).get("vues__sum"),
        "vues_total": Video.objects.filter(chaine=chaine_id).aggregate(Sum('vues')).get("vues__sum"),
        "vulgarisation": Video.objects.filter(chaine=chaine_id).filter(vulgarisation = True).count(),
        "sourcees": Video.objects.filter(chaine=chaine_id).filter(sourcee = True).count(),
        "total": Video.objects.filter(chaine=chaine_id).count(),
    }
    abonnement = ChaineAbonnement.objects.filter(chaine = chaine_id)
    print(abonnement)
    video = Video.objects.filter(chaine=chaine_id)
    return render(request, 'crowd_sourcing/chaine.html', {'chaine': chaine, 'abonnement': abonnement, 'video': video, 'stats': stats} )

# Lister les collectif ou voir les détails d'un
def collectif(request, collectif_id = None):
    if collectif_id == None:
        return render(request, 'crowd_sourcing/collectifs.html', {'collectif': Collectif.objects.all()})

    collectif = get_object_or_404(Collectif, pk=collectif_id)
    chaine = Chaine.objects.filter(collectif=collectif.id)
    return render(request, 'crowd_sourcing/collectif.html', {'collectif': collectif, 'chaine': chaine})

# Ajouter ou lister les sources
def source(request, source_id = None):
    source = get_object_or_404(Source, pk=source_id)
    url = SourceURL.objects.filter(source=source_id)
    return render(request, 'crowd_sourcing/source.html', {'source': source, 'url': url})

# Lister les vidéos et détail d'une vidéo
def video(request, video_id):
    video = get_object_or_404(Video, pk=video_id)
    sources = Source.objects.filter(video = video)
    return render(request, 'crowd_sourcing/video.html', {'video': video, 'source': sources})
