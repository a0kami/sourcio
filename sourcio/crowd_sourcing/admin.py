from django.contrib import admin

from .models import Collectif
from .models import Chaine, ChaineAbonnement, ChaineDomaine, ChaineSexe
from .models import Source, SourceURL
from .models import Video

admin.site.register(Collectif)
admin.site.register(Chaine)
admin.site.register(ChaineAbonnement)
admin.site.register(ChaineDomaine)
admin.site.register(ChaineSexe)
admin.site.register(Source)
admin.site.register(SourceURL)
admin.site.register(Video)
