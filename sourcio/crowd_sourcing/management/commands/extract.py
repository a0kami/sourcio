from django.core.management.base import BaseCommand, CommandError
from crowd_sourcing.models import Chaine, ChaineDomaine, ChaineSexe, Collectif
from apiclient.discovery import build
import argparse, csv, datetime, json, re


class Command(BaseCommand):
    """
    -f Fichier en entrée au format CSV
    -o Fichier de sortie (csv)
    -s Séparateur (§ par défaut)
    -t Time (ne récupère les vidéos que depuis le 01/01/2018)
    """
    help = 'Exporte les données de vidéos en csv'

    def add_arguments(self, parser):
        parser.add_argument('-f', type=str, required=True)
        parser.add_argument('-o', type=argparse.FileType('w'), required=True)
        parser.add_argument('-s', default="§", type=str)
        parser.add_argument('-t', default=False, action='store_true')


    def remove_empty_kwargs(self, **kwargs):
        good_kwargs = {}
        if kwargs is not None:
            for key, value in kwargs.items():
                if value:
                    good_kwargs[key] = value
        return good_kwargs


    def playlistId_by_channelID(self, client, **kwargs):
        kwargs = self.remove_empty_kwargs(**kwargs)

        response = client.channels().list(
        **kwargs
        ).execute()
        print(response)
        return response['items'][0]['contentDetails']['relatedPlaylists']['uploads']

    def playlistItems_by_playlistId(self, client, **kwargs):
        videoIds = []
        kwargs = self.remove_empty_kwargs(**kwargs)

        response = client.playlistItems().list(
        **kwargs
        ).execute()

        print(response['pageInfo']['totalResults'])

        #videoIds += [item['contentDetails']['videoId'] for item in response['items']]
        while(True):

            videoIds += [item['contentDetails']['videoId'] for item in response['items']]
            if("nextPageToken" not in response):
                break
            else:
                kwargs["pageToken"] = response['nextPageToken']
                response = client.playlistItems().list(
                **kwargs
                ).execute()
        #print(json.dumps(response, indent=2))

        return videoIds

    def videoDetails_by_videoIdSet(self, client, **kwargs):
        videosDetails = []
        kwargs = self.remove_empty_kwargs(**kwargs)

        regex = re.compile("PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?")

        videoIdsList = kwargs["id"]
        while len(videoIdsList):

            kwargs["id"] = ",".join(videoIdsList[:50])

            response = client.videos().list(
            **kwargs
            ).execute()

            for video in response['items']:
                id = video['id']
                dateStr = video['snippet']['publishedAt']
                titre = video['snippet']['title']
                dureeStr = video['contentDetails']['duration']
                like = "-"
                dislike = "-"
                comments = "-"
                vues = "-"
                if "viewCount" in video['statistics']:
                    vues = video['statistics']['viewCount']
                if "likeCount" in video['statistics']:
                    like = video['statistics']['likeCount']
                if "dislikeCount" in video['statistics']:
                    dislike = video['statistics']['dislikeCount']
                if "commentCount" in video['statistics']:
                    comments = video['statistics']['commentCount']

                date = datetime.datetime.fromisoformat(dateStr[:-1])

                duree = regex.search(dureeStr).groups()
                duree = [field if field is not None else '0' for field in duree]

                videosDetails.append(["",
                                      titre,
                                      "{heure}:{minute}:{seconde}".format(heure = duree[0], minute = duree[1], seconde = duree[2]),
                                      "",
                                      date,
                                      vues,
                                      like,
                                      dislike,
                                      comments,
                                      id,
                                      ])
                # Chaine, Vidéo, Durée, Durée Moyenne,
                # Date publication, Vues, Likes, Dislikes,
                # Commentaires, Id Vidéo
            videoIdsList = videoIdsList[50:]

        #print(json.dumps(response, indent=2))
        return videosDetails

    def handle(self, *args, **options):
        # Arguments
        trim = options['t']
        janvier2018 = datetime.datetime(2018, 1, 1)
        # Connexion API
        client = build('youtube', 'v3', developerKey='AIzaSyAqkjBtcHaM-O_Lb7tUl3oX1ZswnyAloCI')

        # Ouvrir csv
        csvWriter = csv.writer(options['o'],
            delimiter=options['s'],
            quotechar='"',
            quoting=csv.QUOTE_MINIMAL)

        csvWriter.writerow(["Chaine", "Vidéo", "Durée", "Durée Moyenne", "Date publication", "Vues", "Likes", "Dislikes", "Commentaires", "Id Vidéo"])

        # Pour toute les chaines
        chaines = []
        if(options['f'] == None):
            for chaineRecord in Chaine.objects.all().order_by('nom'):
                chaines.append([chaineRecord.nom, chaineRecord.url.split("/")])
                # url = chaine.url.
        else:
            # Parse csv
            with open(options['f'], 'r') as csvfile:
                reader = csv.reader(csvfile, delimiter=options['s'], quotechar='"')
                for row in reader:
                    chaines.append([row[0],row[2].split("/")])
            chaines = chaines[1:]

        for chaineNom, url in chaines:
            print(chaineNom + ":", end=" ")
            plstId = ""

            if(url[0] == "user"):
                plstId = self.playlistId_by_channelID(client,
                    part='contentDetails',
                    forUsername=url[1],)

            elif(url[0] == "channel"):
                plstId = self.playlistId_by_channelID(client,
                    part='contentDetails',
                    id=url[1],)

            else:
                print("ERREUR URL CHAINE")
                continue

            # Récupérer les vidéos d'un chaîne
            vdIds = self.playlistItems_by_playlistId(client,
                part='contentDetails',
                maxResults=50,
                playlistId=plstId)

            # Récupérer les données
            vdDtls = self.videoDetails_by_videoIdSet(client,
                part='snippet,contentDetails,statistics',
                maxResults=50,
                id=vdIds)

            # Vérifier date
            print("\tDétails de {0} vidéos trouvés".format(len(vdDtls)))
            for details in vdDtls:
                # Skip si date > 01/01/2018
                if options["t"]:
                    if details[4] > janvier2018:
                        continue

                details[4] = details[4].strftime("%d/%m/%y %H:%M:%S")
                details[0] = chaineNom
                csvWriter.writerow(details)
