from django.core.management.base import BaseCommand, CommandError
import argparse
#from polls.models import Question as Poll
from crowd_sourcing.models import Chaine, ChaineDomaine, ChaineSexe, Collectif

class Command(BaseCommand):
    help = 'Remplit la base de données avec les données du fichier'

    def add_arguments(self, parser):
        parser.add_argument('-f', type=argparse.FileType('r'), required=True)
        parser.add_argument('-s', default="§", type=str)

    def handle(self, *args, **options):
        # Lire
        data = options['f'].read().split("\n")
        for line in data[1:]:
            if len(line) == 0:
                continue
            fields = line.split(options['s'])
            nom = fields[0]
            url = fields[1]
            observation = fields[2]
            sexeCode = fields[4]
            domaineCode = fields[6]
            discipline = fields[7]
            if fields[9] != "0" :
                jour, mois, annee = fields[8].split("/")
                date_creation = "20{annee}-{mois}-{jour}".format(jour = jour, mois = mois, annee = annee)
            else:
                date_creation = None

            # Écrire
            chaine = Chaine.objects.create(
                nom = nom,
                url = url,
                date_creation = date_creation,
                collectif = Collectif.objects.get(pk=1),
                sexe_presentateur = ChaineSexe.objects.get(valeur=sexeCode),
                domaine = ChaineDomaine.objects.get(valeur=domaineCode),
                observation = observation,
            )
            print("Added {nom}..".format(nom = nom))
