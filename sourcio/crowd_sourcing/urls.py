from django.urls import path

from . import views

app_name = 'crowd_sourcing'
urlpatterns = [
    path('', views.index, name="index"),
    path('chaine/', views.chaine, name="chaine"),
    path('chaine/<int:chaine_id>', views.chaine, name="chaine"),
    path('collectif/', views.collectif, name="collectif"),
    path('collectif/<int:collectif_id>', views.collectif, name="collectif"),
    path('source/<int:source_id>', views.source, name="source"),
    path('source/', views.source, name="source"),
    path('video/<int:video_id>', views.video, name="video"),
]
