from django.apps import AppConfig


class CrowdSourcingConfig(AppConfig):
    name = 'crowd_sourcing'
